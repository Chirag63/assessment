﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assignment_1;

namespace UnitTestAssignment
{
    [TestClass]
    public class UnitTest1
    {
        //Initialize Insstance of shape Control class
        DrawShape c = new DrawShape();
        
        //Method for 1st unit test of Draw Line
        [TestMethod]
        public void TestDrawLine()
        {            
            c.DrawLine(100, 200);
            //Checking Unit test value which return true if method successfully execute the code otherwise false
            Assert.IsTrue(ShapeValue.isUnitTestValid);
        }
        //Method for 1st unit test of Draw Circle
        [TestMethod]
        public void TestCircle()
        {
            c.DrawCircle(50);
            //Checking Unit test value which return true if method successfully execute the code otherwise false
            Assert.IsTrue(ShapeValue.isUnitTestValid);
        }
        //Method for 1st unit test of Draw Square
        [TestMethod]
        public void TestSquare()
        {
            c.DrawSquare(50);
            //Checking Unit test value which return true if method successfully execute the code otherwise false
            Assert.IsTrue(ShapeValue.isUnitTestValid);
        }
        //Method for 1st unit test of Draw Rectangle
        [TestMethod]
        public void TestRectangle()
        {
            c.DrawRect(100, 200);
            //Checking Unit test value which return true if method successfully execute the code otherwise false
            Assert.IsTrue(ShapeValue.isUnitTestValid);
        }
        //Method for 1st unit test of Draw Triangle
        [TestMethod]
        public void TestTriangle()
        {
            c.DrawTriangle(120, 150,170);
            //Checking Unit test value which return true if method successfully execute the code otherwise false
            Assert.IsTrue(ShapeValue.isUnitTestValid);
        }
        //Method for 1st unit test of Move
        [TestMethod]
        public void TestMove()
        {
            c.MovePoint(120, 150);
            //Checking Unit test value which return true if method successfully execute the code otherwise false
            Assert.IsTrue(ShapeValue.isUnitTestValid);
        }
    }
}
