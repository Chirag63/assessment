﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1
{
    public class ShapeValue
    {
        //Variable declaration using getset method
        
        //current position
        private static int _x,_y;
        //
        private static Bitmap _NewPicture= new Bitmap(640,480);
        //Brush for fillcolor 
        private static SolidBrush _FillColor;
        //Create Boolean variable for fill, unit resting and drawing 
        private static Boolean _isFill,_isUnitTestValid,_isDrawing;
        //crate variable pen for drawing 
        private static Pen _pen, _shapeBorder;
        //Graphics variable to execute graphics fuction
        private static Graphics _g;
        //declare current position and current shape recangle to show current pointer
        private static Rectangle _CuurPos, _CurrShape;
                
        //following are get set method for using these variavble globally in project
        public static Boolean isFill
        {
            get
            {
                return _isFill;
            }
            set { _isFill = value; }
        }

        public static Boolean isDrawing
        {
            get
            {
                return _isDrawing;
            }
            set { _isDrawing = value; }
        }

        public static Graphics g
        {
            get { return _g; }
            set { _g = value; }
        }
        public static Rectangle CuurPos
        {
            get
            {
                return _CuurPos;
            }
            set { _CuurPos = value; }
        }
        public static Rectangle CurrShape
        {
            get
            {
                return _CurrShape;
            }
            set { _CurrShape = value; }
        }

        public static SolidBrush FillColor
        {
            get
            {
                return _FillColor;
            }
            set { _FillColor = value; }
        }

        public static Bitmap NewPicture 
        {
            get {
                return _NewPicture; }   
            set { _NewPicture = value; }  
        }

        public static int x   
        {
            get
            {   return _x; }   
            set { _x = value; }  
        }

        public static int y
        {
            get
            { return _y; }
            set { _y = value; }
        }        

        public static Pen pen {
            get { return _pen; }
            set { _pen = value; }
        }
        public static Pen shapeBorder
        {
            get { return _shapeBorder; }
            set { _shapeBorder = value; }
        }

        public static Boolean isUnitTestValid
        {
            get { return _isUnitTestValid; }
            set { _isUnitTestValid = value; }
        }
        

    }
}
