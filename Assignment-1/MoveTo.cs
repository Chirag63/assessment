﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Assignment_1
{
    public class MoveTo : Shapes
    {
        public string drawShape(int x, int y)
        {
            try
            {
                Pen pen = new Pen(SystemColors.ActiveBorder, 2);
                ShapeValue.g.DrawRectangle(pen, ShapeValue.CuurPos);
                pen = new Pen(Color.Red, 2);
                ShapeValue.CuurPos = new Rectangle(x, y, 2, 2);
                ShapeValue.g.DrawRectangle(pen, ShapeValue.CuurPos);
                ShapeValue.x = x;
                ShapeValue.y = y;
                ShapeValue.isUnitTestValid = true;
                return "";
            }
            catch (Exception ex)
            {               
                ShapeValue.isUnitTestValid = false;
                return ex.Message.Trim();
            }
        }
    }
}
