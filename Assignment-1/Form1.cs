﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media.Imaging;


namespace Assignment_1
{
    public partial class FrmAssignment : Form
    {
        //Declare object for Shape control class
        ShapeCtrl d;
        //Drawing status 
        bool isDraw;
        // our collection of point for drawing
        List<List<Point>> _pointcoll = new List<List<Point>>();
        // the current point being drawn
        List<Point> _currpoint;
        public FrmAssignment()
        {               
            InitializeComponent();
            //select color default is black
            SelectedColor(btnBlack);       
            //Assign Vlaue to pen
            ShapeValue.pen = new Pen (Color.FromName(btnBlack.Tag.ToString().Trim()),1);
            //Assign point x and y as 0
            ShapeValue.x = ShapeValue.y = 0;
            //create object of Shape control class
            d = new ShapeCtrl();

            ShapeValue.g = Graphics.FromImage(ShapeValue.NewPicture);
            ShapeValue.shapeBorder = new Pen(Color.Black, 1);

            //Set currunt point
            d.CurrPoint(false);
            //Refresh Picturebox
            Refresh();          
            //Assign Default focus to "Single Line Command"
            txtOneCommand.Focus();
        }

        private void picDrawer1_Paint(object sender, PaintEventArgs e)
        {            
            if (ShapeValue.isDrawing)
            {                
                //Graphics object with current Bitmap
                Graphics g = Graphics.FromImage(ShapeValue.NewPicture);
                g.SmoothingMode = SmoothingMode.AntiAlias;//Activate Smoothingmode
                //Draw on picturebox 
                foreach (List<Point> pnt in _pointcoll.Where(x => x.Count > 1))
                    g.DrawLines(ShapeValue.pen, pnt.ToArray());                
            }
            //Set Drawing into picture box
            e.Graphics.DrawImageUnscaled(ShapeValue.NewPicture, 0, 0);
        }      

        private void picDrawer1_MouseDown(object sender, MouseEventArgs e)
        {
            if (ShapeValue.isDrawing)
            {
                isDraw = true;
                // mouse is down, starting new point
                _currpoint = new List<Point>();
                // add the initial point to the new point
                _currpoint.Add(e.Location);
                // add the new point collection to our pointcoll array
                _pointcoll.Add(_currpoint);
            }
        }

        private void picDrawer1_MouseMove(object sender, MouseEventArgs e)
        {            
            if (isDraw)
            {
                // add current point if we're in drawing mode
                _currpoint.Add(e.Location);
                Refresh(); // refresh the picturebox to see the latest section
            }
        }

        private void picDrawer1_MouseUp(object sender, MouseEventArgs e)
        {
            isDraw = false;
            _pointcoll = new List<List<Point>>();
            _currpoint = null;
        }

        private void txtOneCommand_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {                
                if (ShapeValue.isDrawing)
                {
                    MessageBox.Show("Drawing mode is enable!");
                }
                else
                {
                    Boolean runFlag = true;
                    if (txtOneCommand.Text.Trim() != string.Empty)
                        d.RunCommand(txtOneCommand.Text.Trim(),0, true,ref runFlag);
                    else
                        MessageBox.Show("Please enter any command!");
                }
                txtOneCommand.Text = string.Empty;
                Refresh();

                txtOneCommand.Focus();                
            }
        }

        public override void Refresh()
        {
            picDrawer1.Image = ShapeValue.NewPicture;           
        }       

        private void btnExecute_Click(object sender, EventArgs e)
        {
            Boolean flg = false;
            if (ShapeValue.isDrawing)
            {
                MessageBox.Show("Drawing mode is enable!");
            }
            else
            {
                if (txtMultiCommand.Text.Trim() != string.Empty)
                {
                    d.RunMultiCommands(txtMultiCommand.Text.Trim(),true);
                    txtMultiCommand.Focus();
                    txtMultiCommand.Text = string.Empty;
                    flg = true;
                }
                if (txtOneCommand.Text.Trim() != string.Empty)
                {
                    Boolean cmdstatus = true;
                    d.RunCommand(txtOneCommand.Text.Trim(),0,true,ref cmdstatus);
                    txtOneCommand.Focus();
                    txtOneCommand.Text = string.Empty;
                    flg = true;
                }
                if (!flg)
                {
                    MessageBox.Show("Please enter any command!");
                    txtOneCommand.Focus();
                }
                Refresh();
            }
        }

        private void btnSyntex_Click(object sender, EventArgs e)
        {
            Boolean flg = false;
            if (ShapeValue.isDrawing)
            {
                MessageBox.Show("Drawing mode is enable!");
            }
            else
            {
                if (txtMultiCommand.Text.Trim() != string.Empty)
                {
                    d.RunMultiCommands(txtMultiCommand.Text.Trim(), false);
                    txtMultiCommand.Focus();
                    txtMultiCommand.Text = string.Empty;
                    flg = true;
                }                
                if (!flg)
                {
                    MessageBox.Show("Please enter any command!");
                    txtOneCommand.Focus();
                }
                Refresh();
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ShapeValue.NewPicture = new Bitmap(640, 480);
            d = new ShapeCtrl();
            d.CurrPoint(true);
            Refresh();
            SelectedColor(btnBlack);
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            ShapeValue.NewPicture = new Bitmap(640, 480);
            ShapeValue.g = Graphics.FromImage(ShapeValue.NewPicture);
            ShapeValue.shapeBorder = new Pen(Color.Black, 1);
            ShapeValue.x = ShapeValue.y = 0;            
            d = new ShapeCtrl();
            d.CurrPoint(false);
            Refresh();
            SelectedColor(btnBlack);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "JPEG|*.jpeg";
            dialog.Title = "AssignmentTest1";
            dialog.ShowDialog();
            if (dialog.FileName != "" && ShapeValue.NewPicture != null)
            {
                ShapeValue.NewPicture.Save(dialog.FileName);
            }
        }

        private void btnRed_Click(object sender, EventArgs e)
        {
            ColorClick(btnRed);
        }

        private void btnOrange_Click(object sender, EventArgs e)
        {
            ColorClick(btnOrange);
        }

        private void btnYellow_Click(object sender, EventArgs e)
        {
            ColorClick(btnYellow);
        }

        private void btnLime_Click(object sender, EventArgs e)
        {
            ColorClick(btnLime);
        }

        private void btnCyan_Click(object sender, EventArgs e)
        {
            ColorClick(btnCyan);
        }

        private void btnBlue_Click(object sender, EventArgs e)
        {
            ColorClick(btnBlue);
        }

        private void btnBlack_Click(object sender, EventArgs e)
        {
            ColorClick(btnBlack);
        }

        private void btnHighlight_Click(object sender, EventArgs e)
        {
            ColorClick(btnHighlight);
        }

        private void btnOlive_Click(object sender, EventArgs e)
        {
            ColorClick(btnOlive);
        }

        private void btnGreen_Click(object sender, EventArgs e)
        {
            ColorClick(btnGreen);
        }

        private void btnSteelBlue_Click(object sender, EventArgs e)
        {
            ColorClick(btnSteelBlue);
        }

        private void btnFuchsia_Click(object sender, EventArgs e)
        {
            ColorClick(btnFuchsia);
        }

        private void chkFill_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFill.Checked == true)
                ShapeValue.isFill = true;
            else
                ShapeValue.isFill = false;
        }

        private void ColorClick(Button btn)
        {
            Color selclr = Color.FromName(btn.Tag.ToString().Trim());
            ShapeValue.FillColor = new SolidBrush(selclr);
            ShapeValue.pen = new Pen(selclr,1);
            btnPen.BackColor = selclr;
            SelectedColor(btn);
            
        }

        private void SelectedColor(Button btn)
        {
            btnRed.FlatStyle = FlatStyle.Standard;
            btnOrange.FlatStyle = FlatStyle.Standard;
            btnYellow.FlatStyle = FlatStyle.Standard;
            btnLime.FlatStyle = FlatStyle.Standard;
            btnCyan.FlatStyle = FlatStyle.Standard;
            btnBlue.FlatStyle = FlatStyle.Standard;
            btnBlack.FlatStyle = FlatStyle.Standard;
            btnHighlight.FlatStyle = FlatStyle.Standard;
            btnOlive.FlatStyle = FlatStyle.Standard;
            btnGreen.FlatStyle = FlatStyle.Standard;
            btnSteelBlue.FlatStyle = FlatStyle.Standard;
            btnFuchsia.FlatStyle = FlatStyle.Standard;
            btn.FlatStyle = FlatStyle.Popup;
        }

        private void btnPen_Click(object sender, EventArgs e)
        {
            if (ShapeValue.isDrawing)
            {   
                ShapeValue.isDrawing = false;
                btnPen.FlatStyle = FlatStyle.Standard;
            }
            else
            {
                ShapeValue.isDrawing = true;
                btnPen.FlatStyle = FlatStyle.Popup;
            }

        }
       
    }
}
