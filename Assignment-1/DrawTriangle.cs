﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Assignment_1
{
    public class DrawTriangle : Shapes
    {
        public string drawShape(int x, int y)
        {
            try
            {
                int tx, ty, cx, cy;
                cx = Convert.ToInt32(ShapeValue.x - (x / 3));
                cy = Convert.ToInt32(ShapeValue.y - (y / 3));
                tx = Convert.ToInt32(cx + x);
                ty = Convert.ToInt32(cy + y);

                Point[] points = new Point[3];
                points[0] = new Point(cx, cy);
                points[1] = new Point(tx, cy);
                points[2] = new Point(cx, ty);
                if (ShapeValue.isFill)
                {
                    ShapeValue.g.FillPolygon(ShapeValue.FillColor, points);
                    ShapeValue.g.DrawPolygon(ShapeValue.shapeBorder, points);
                }
                else
                    ShapeValue.g.DrawPolygon(ShapeValue.pen, points);
                ShapeValue.isUnitTestValid = true;
                return "";
            }
            catch (Exception ex)
            {                
                ShapeValue.isUnitTestValid = false;
                return ex.Message.Trim();
            }
        }
    }
}
