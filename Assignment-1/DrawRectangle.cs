﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Assignment_1
{
    public class DrawRectangle : Shapes
    {
        public string drawShape(int x, int y)
        {
            try
            {
                int xpos = ShapeValue.x - (x/ 2);
                int ypos = ShapeValue.y - (y/ 2);
                ShapeValue.CurrShape = new Rectangle(xpos, ypos, x, y);
                if (ShapeValue.isFill)
                {
                    ShapeValue.g.FillRectangle(ShapeValue.FillColor, ShapeValue.CurrShape);
                    ShapeValue.g.DrawRectangle(ShapeValue.shapeBorder, ShapeValue.CurrShape);
                }
                else
                {
                    ShapeValue.g.DrawRectangle(ShapeValue.pen, ShapeValue.CurrShape);
                }
                ShapeValue.isUnitTestValid = true;
                return "";
            }
            catch (Exception ex)
            {                
                ShapeValue.isUnitTestValid = false;
                return ex.Message.Trim();
            }
        }
    }
}
