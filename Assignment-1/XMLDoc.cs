﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Assignment_1
{
    public class XMLDoc
    {
        static void XMLWritr()
        {
            XmlDocument doc = new XmlDocument();
            XmlText txt;
            //(1) the xml declaration is recommended, but not mandatory
            XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement root = doc.DocumentElement;
            doc.InsertBefore(xmlDeclaration, root);

            //(2) string.Empty makes cleaner code
            XmlElement element1 = doc.CreateElement(string.Empty, "Functionality", string.Empty);
            doc.AppendChild(element1);

            XmlElement drLine = doc.CreateElement(string.Empty, "Line", string.Empty);
            element1.AppendChild(drLine);

            XmlElement  Linefn= doc.CreateElement(string.Empty, "public string drawShape(int x, int y) ", string.Empty);
            txt = doc.CreateTextNode("{");
            Linefn.AppendChild(txt);
            drLine.AppendChild(Linefn);

            XmlElement Linetry = doc.CreateElement(string.Empty, "try", string.Empty);
            txt = doc.CreateTextNode("{");
            Linetry.AppendChild(txt);
            txt = doc.CreateTextNode("ShapeValue.g.DrawLine(ShapeValue.pen, ShapeValue.x, ShapeValue.y, x, y);");
            Linetry.AppendChild(txt);
            txt = doc.CreateTextNode("ShapeValue.isUnitTestValid = true;");
            Linetry.AppendChild(txt);
            txt = doc.CreateTextNode("return '';");
            Linetry.AppendChild(txt);
            txt = doc.CreateTextNode("}");
            Linetry.AppendChild(txt);
            txt = doc.CreateTextNode("catch (Exception ex)");
            Linetry.AppendChild(txt);
            txt = doc.CreateTextNode("{");
            Linetry.AppendChild(txt);
            txt = doc.CreateTextNode("ShapeValue.isUnitTestValid = false;");
            Linetry.AppendChild(txt);
            txt = doc.CreateTextNode("return ex.Message.Trim();");
            Linetry.AppendChild(txt);
            txt = doc.CreateTextNode("}");
            Linetry.AppendChild(txt);
            txt = doc.CreateTextNode("}");
            Linetry.AppendChild(txt);
            drLine.AppendChild(Linetry);
            doc.Save("D:\\document.xml");
        }
    }
}
