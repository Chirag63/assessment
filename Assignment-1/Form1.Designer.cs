﻿namespace Assignment_1
{
    partial class FrmAssignment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAssignment));
            this.picDrawer1 = new System.Windows.Forms.PictureBox();
            this.txtOneCommand = new System.Windows.Forms.TextBox();
            this.txtMultiCommand = new System.Windows.Forms.TextBox();
            this.txtNote = new System.Windows.Forms.TextBox();
            this.lblMultiCommand = new System.Windows.Forms.Label();
            this.lblSingalCommand = new System.Windows.Forms.Label();
            this.btnExecute = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnFuchsia = new System.Windows.Forms.Button();
            this.btnCyan = new System.Windows.Forms.Button();
            this.btnSteelBlue = new System.Windows.Forms.Button();
            this.btnOrange = new System.Windows.Forms.Button();
            this.btnGreen = new System.Windows.Forms.Button();
            this.btnLime = new System.Windows.Forms.Button();
            this.btnOlive = new System.Windows.Forms.Button();
            this.btnYellow = new System.Windows.Forms.Button();
            this.btnHighlight = new System.Windows.Forms.Button();
            this.btnBlue = new System.Windows.Forms.Button();
            this.btnBlack = new System.Windows.Forms.Button();
            this.btnRed = new System.Windows.Forms.Button();
            this.btnPen = new System.Windows.Forms.Button();
            this.chkFill = new System.Windows.Forms.CheckBox();
            this.btnSyntex = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.picDrawer1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // picDrawer1
            // 
            this.picDrawer1.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.picDrawer1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.picDrawer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picDrawer1.Location = new System.Drawing.Point(0, 0);
            this.picDrawer1.Name = "picDrawer1";
            this.picDrawer1.Size = new System.Drawing.Size(515, 470);
            this.picDrawer1.TabIndex = 0;
            this.picDrawer1.TabStop = false;
            this.picDrawer1.Paint += new System.Windows.Forms.PaintEventHandler(this.picDrawer1_Paint);
            this.picDrawer1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picDrawer1_MouseDown);
            this.picDrawer1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.picDrawer1_MouseMove);
            this.picDrawer1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picDrawer1_MouseUp);
            // 
            // txtOneCommand
            // 
            this.txtOneCommand.Location = new System.Drawing.Point(521, 237);
            this.txtOneCommand.Name = "txtOneCommand";
            this.txtOneCommand.Size = new System.Drawing.Size(299, 20);
            this.txtOneCommand.TabIndex = 1;
            this.txtOneCommand.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtOneCommand_KeyDown);
            // 
            // txtMultiCommand
            // 
            this.txtMultiCommand.Location = new System.Drawing.Point(521, 21);
            this.txtMultiCommand.Multiline = true;
            this.txtMultiCommand.Name = "txtMultiCommand";
            this.txtMultiCommand.Size = new System.Drawing.Size(299, 192);
            this.txtMultiCommand.TabIndex = 2;
            // 
            // txtNote
            // 
            this.txtNote.BackColor = System.Drawing.SystemColors.Control;
            this.txtNote.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNote.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNote.ForeColor = System.Drawing.Color.Red;
            this.txtNote.Location = new System.Drawing.Point(526, 413);
            this.txtNote.Multiline = true;
            this.txtNote.Name = "txtNote";
            this.txtNote.ReadOnly = true;
            this.txtNote.Size = new System.Drawing.Size(288, 29);
            this.txtNote.TabIndex = 3;
            this.txtNote.Text = "Note: All command should be separate by semicolon (\";\").";
            // 
            // lblMultiCommand
            // 
            this.lblMultiCommand.AutoSize = true;
            this.lblMultiCommand.Location = new System.Drawing.Point(521, 5);
            this.lblMultiCommand.Name = "lblMultiCommand";
            this.lblMultiCommand.Size = new System.Drawing.Size(118, 13);
            this.lblMultiCommand.TabIndex = 4;
            this.lblMultiCommand.Text = "Multiple Command Area";
            // 
            // lblSingalCommand
            // 
            this.lblSingalCommand.AutoSize = true;
            this.lblSingalCommand.Location = new System.Drawing.Point(521, 221);
            this.lblSingalCommand.Name = "lblSingalCommand";
            this.lblSingalCommand.Size = new System.Drawing.Size(109, 13);
            this.lblSingalCommand.TabIndex = 5;
            this.lblSingalCommand.Text = "Single Line Command";
            // 
            // btnExecute
            // 
            this.btnExecute.Location = new System.Drawing.Point(521, 357);
            this.btnExecute.Name = "btnExecute";
            this.btnExecute.Size = new System.Drawing.Size(94, 27);
            this.btnExecute.TabIndex = 6;
            this.btnExecute.Text = "Execute";
            this.btnExecute.UseVisualStyleBackColor = true;
            this.btnExecute.Click += new System.EventHandler(this.btnExecute_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(615, 357);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(103, 27);
            this.btnClear.TabIndex = 8;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(718, 357);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(99, 27);
            this.btnReset.TabIndex = 9;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(521, 384);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(296, 24);
            this.btnSave.TabIndex = 12;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnFuchsia);
            this.panel1.Controls.Add(this.btnCyan);
            this.panel1.Controls.Add(this.btnSteelBlue);
            this.panel1.Controls.Add(this.btnOrange);
            this.panel1.Controls.Add(this.btnGreen);
            this.panel1.Controls.Add(this.btnLime);
            this.panel1.Controls.Add(this.btnOlive);
            this.panel1.Controls.Add(this.btnYellow);
            this.panel1.Controls.Add(this.btnHighlight);
            this.panel1.Controls.Add(this.btnBlue);
            this.panel1.Controls.Add(this.btnBlack);
            this.panel1.Controls.Add(this.btnRed);
            this.panel1.Controls.Add(this.btnPen);
            this.panel1.Location = new System.Drawing.Point(521, 260);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(299, 62);
            this.panel1.TabIndex = 13;
            // 
            // btnFuchsia
            // 
            this.btnFuchsia.BackColor = System.Drawing.Color.Fuchsia;
            this.btnFuchsia.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFuchsia.Location = new System.Drawing.Point(256, 29);
            this.btnFuchsia.Name = "btnFuchsia";
            this.btnFuchsia.Size = new System.Drawing.Size(37, 30);
            this.btnFuchsia.TabIndex = 12;
            this.btnFuchsia.Tag = "Fuchsia";
            this.btnFuchsia.UseVisualStyleBackColor = false;
            this.btnFuchsia.Click += new System.EventHandler(this.btnFuchsia_Click);
            // 
            // btnCyan
            // 
            this.btnCyan.BackColor = System.Drawing.Color.Cyan;
            this.btnCyan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCyan.Location = new System.Drawing.Point(220, 0);
            this.btnCyan.Name = "btnCyan";
            this.btnCyan.Size = new System.Drawing.Size(37, 30);
            this.btnCyan.TabIndex = 11;
            this.btnCyan.Tag = "Cyan";
            this.btnCyan.UseVisualStyleBackColor = false;
            this.btnCyan.Click += new System.EventHandler(this.btnCyan_Click);
            // 
            // btnSteelBlue
            // 
            this.btnSteelBlue.BackColor = System.Drawing.Color.SteelBlue;
            this.btnSteelBlue.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSteelBlue.Location = new System.Drawing.Point(220, 29);
            this.btnSteelBlue.Name = "btnSteelBlue";
            this.btnSteelBlue.Size = new System.Drawing.Size(37, 30);
            this.btnSteelBlue.TabIndex = 10;
            this.btnSteelBlue.Tag = "SteelBlue";
            this.btnSteelBlue.UseVisualStyleBackColor = false;
            this.btnSteelBlue.Click += new System.EventHandler(this.btnSteelBlue_Click);
            // 
            // btnOrange
            // 
            this.btnOrange.BackColor = System.Drawing.Color.DarkOrange;
            this.btnOrange.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOrange.Location = new System.Drawing.Point(112, 0);
            this.btnOrange.Name = "btnOrange";
            this.btnOrange.Size = new System.Drawing.Size(37, 30);
            this.btnOrange.TabIndex = 9;
            this.btnOrange.Tag = "DarkOrange";
            this.btnOrange.UseVisualStyleBackColor = false;
            this.btnOrange.Click += new System.EventHandler(this.btnOrange_Click);
            // 
            // btnGreen
            // 
            this.btnGreen.BackColor = System.Drawing.Color.Green;
            this.btnGreen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGreen.Location = new System.Drawing.Point(184, 29);
            this.btnGreen.Name = "btnGreen";
            this.btnGreen.Size = new System.Drawing.Size(37, 30);
            this.btnGreen.TabIndex = 8;
            this.btnGreen.Tag = "Green";
            this.btnGreen.UseVisualStyleBackColor = false;
            this.btnGreen.Click += new System.EventHandler(this.btnGreen_Click);
            // 
            // btnLime
            // 
            this.btnLime.BackColor = System.Drawing.Color.Lime;
            this.btnLime.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLime.Location = new System.Drawing.Point(184, 0);
            this.btnLime.Name = "btnLime";
            this.btnLime.Size = new System.Drawing.Size(37, 30);
            this.btnLime.TabIndex = 7;
            this.btnLime.Tag = "Lime";
            this.btnLime.UseVisualStyleBackColor = false;
            this.btnLime.Click += new System.EventHandler(this.btnLime_Click);
            // 
            // btnOlive
            // 
            this.btnOlive.BackColor = System.Drawing.Color.Olive;
            this.btnOlive.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOlive.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOlive.Location = new System.Drawing.Point(148, 29);
            this.btnOlive.Name = "btnOlive";
            this.btnOlive.Size = new System.Drawing.Size(37, 30);
            this.btnOlive.TabIndex = 6;
            this.btnOlive.Tag = "Olive";
            this.btnOlive.UseVisualStyleBackColor = false;
            this.btnOlive.Click += new System.EventHandler(this.btnOlive_Click);
            // 
            // btnYellow
            // 
            this.btnYellow.BackColor = System.Drawing.Color.Yellow;
            this.btnYellow.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnYellow.Location = new System.Drawing.Point(148, 0);
            this.btnYellow.Name = "btnYellow";
            this.btnYellow.Size = new System.Drawing.Size(37, 30);
            this.btnYellow.TabIndex = 5;
            this.btnYellow.Tag = "Yellow";
            this.btnYellow.UseVisualStyleBackColor = false;
            this.btnYellow.Click += new System.EventHandler(this.btnYellow_Click);
            // 
            // btnHighlight
            // 
            this.btnHighlight.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnHighlight.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnHighlight.Location = new System.Drawing.Point(112, 29);
            this.btnHighlight.Name = "btnHighlight";
            this.btnHighlight.Size = new System.Drawing.Size(37, 30);
            this.btnHighlight.TabIndex = 4;
            this.btnHighlight.Tag = "Highlight";
            this.btnHighlight.UseVisualStyleBackColor = false;
            this.btnHighlight.Click += new System.EventHandler(this.btnHighlight_Click);
            // 
            // btnBlue
            // 
            this.btnBlue.BackColor = System.Drawing.Color.Blue;
            this.btnBlue.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBlue.Location = new System.Drawing.Point(256, 0);
            this.btnBlue.Name = "btnBlue";
            this.btnBlue.Size = new System.Drawing.Size(37, 30);
            this.btnBlue.TabIndex = 3;
            this.btnBlue.Tag = "Blue";
            this.btnBlue.UseVisualStyleBackColor = false;
            this.btnBlue.Click += new System.EventHandler(this.btnBlue_Click);
            // 
            // btnBlack
            // 
            this.btnBlack.BackColor = System.Drawing.Color.Black;
            this.btnBlack.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBlack.ForeColor = System.Drawing.SystemColors.Control;
            this.btnBlack.Location = new System.Drawing.Point(76, 29);
            this.btnBlack.Name = "btnBlack";
            this.btnBlack.Size = new System.Drawing.Size(37, 30);
            this.btnBlack.TabIndex = 2;
            this.btnBlack.Tag = "Black";
            this.btnBlack.UseVisualStyleBackColor = false;
            this.btnBlack.Click += new System.EventHandler(this.btnBlack_Click);
            // 
            // btnRed
            // 
            this.btnRed.BackColor = System.Drawing.Color.Red;
            this.btnRed.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRed.Location = new System.Drawing.Point(76, 0);
            this.btnRed.Name = "btnRed";
            this.btnRed.Size = new System.Drawing.Size(37, 30);
            this.btnRed.TabIndex = 1;
            this.btnRed.Tag = "Red";
            this.btnRed.UseVisualStyleBackColor = false;
            this.btnRed.Click += new System.EventHandler(this.btnRed_Click);
            // 
            // btnPen
            // 
            this.btnPen.BackColor = System.Drawing.SystemColors.Control;
            this.btnPen.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPen.BackgroundImage")));
            this.btnPen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnPen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPen.Location = new System.Drawing.Point(6, 0);
            this.btnPen.Name = "btnPen";
            this.btnPen.Size = new System.Drawing.Size(71, 59);
            this.btnPen.TabIndex = 0;
            this.btnPen.UseVisualStyleBackColor = false;
            this.btnPen.Click += new System.EventHandler(this.btnPen_Click);
            // 
            // chkFill
            // 
            this.chkFill.AutoSize = true;
            this.chkFill.Location = new System.Drawing.Point(740, 217);
            this.chkFill.Name = "chkFill";
            this.chkFill.Size = new System.Drawing.Size(38, 17);
            this.chkFill.TabIndex = 14;
            this.chkFill.Text = "Fill";
            this.chkFill.UseVisualStyleBackColor = true;
            this.chkFill.CheckedChanged += new System.EventHandler(this.chkFill_CheckedChanged);
            // 
            // btnSyntex
            // 
            this.btnSyntex.Location = new System.Drawing.Point(521, 332);
            this.btnSyntex.Name = "btnSyntex";
            this.btnSyntex.Size = new System.Drawing.Size(296, 25);
            this.btnSyntex.TabIndex = 15;
            this.btnSyntex.Text = "Check Syntex";
            this.btnSyntex.UseVisualStyleBackColor = true;
            this.btnSyntex.Click += new System.EventHandler(this.btnSyntex_Click);
            // 
            // FrmAssignment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(821, 468);
            this.Controls.Add(this.btnSyntex);
            this.Controls.Add(this.chkFill);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnExecute);
            this.Controls.Add(this.lblSingalCommand);
            this.Controls.Add(this.lblMultiCommand);
            this.Controls.Add(this.txtNote);
            this.Controls.Add(this.txtMultiCommand);
            this.Controls.Add(this.txtOneCommand);
            this.Controls.Add(this.picDrawer1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FrmAssignment";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Assignment-1";
            ((System.ComponentModel.ISupportInitialize)(this.picDrawer1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picDrawer1;
        private System.Windows.Forms.TextBox txtOneCommand;
        private System.Windows.Forms.TextBox txtMultiCommand;
        private System.Windows.Forms.TextBox txtNote;
        private System.Windows.Forms.Label lblMultiCommand;
        private System.Windows.Forms.Label lblSingalCommand;
        private System.Windows.Forms.Button btnExecute;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnReset;        
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnFuchsia;
        private System.Windows.Forms.Button btnCyan;
        private System.Windows.Forms.Button btnSteelBlue;
        private System.Windows.Forms.Button btnOrange;
        private System.Windows.Forms.Button btnGreen;
        private System.Windows.Forms.Button btnLime;
        private System.Windows.Forms.Button btnOlive;
        private System.Windows.Forms.Button btnYellow;
        private System.Windows.Forms.Button btnHighlight;
        private System.Windows.Forms.Button btnBlack;
        private System.Windows.Forms.Button btnRed;
        private System.Windows.Forms.Button btnPen;
        private System.Windows.Forms.Button btnBlue;
        private System.Windows.Forms.CheckBox chkFill;
        private System.Windows.Forms.Button btnSyntex;
    }
}

