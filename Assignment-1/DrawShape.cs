﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Reflection;

namespace Assignment_1
{
    public class DrawShape
    {
        //Variable declaration
        //Graphics variable to execute graphics fuction
        Graphics g;
        //declaire pen variable
        Pen shapeBorder;
        //declare two integer variable x and y which show currunt position of ponter 
        int x, y;
        //declare current position recangle to show current pointer
        Rectangle CuurPos;
        //declare current shape recangle to for getting current shape
        Rectangle CurrShape;
        
        public DrawShape()
        {
            //Initialize graphics instance and asign value Bitmap value 
            this.g = Graphics.FromImage(ShapeValue.NewPicture);
            //Initializa pen as black            
            shapeBorder = new Pen(Color.Black, 1);
            ShapeValue.pen = new Pen(Color.Black, 1);
            //Assign Value to x and y from get set method
            x = ShapeValue.x;
            y = ShapeValue.y;            
        }

        public void DrawLine(int xpos, int ypos)
        {
            try
            {                
                this.g.DrawLine(ShapeValue.pen, x, y, xpos, ypos);
                ShapeValue.isUnitTestValid = true;
            }
            catch (Exception ex)
            {
                //print message if any run time error occure
                PrintMessage(ex.Message);
                ShapeValue.isUnitTestValid = false;
            }

        }

        public void DrawSquare(int width)
        {
            try
            {
                int xpos = x - (width / 2);
                int ypos = y - (width / 2);
                CurrShape = new Rectangle(xpos, ypos, width, width);
                if (ShapeValue.isFill)
                {
                    this.g.FillRectangle(ShapeValue.FillColor, CurrShape);
                    this.g.DrawRectangle(shapeBorder, CurrShape);
                }
                else
                    this.g.DrawRectangle(ShapeValue.pen, CurrShape);

                ShapeValue.isUnitTestValid = true;
            }
            catch (Exception ex)
            {
                PrintMessage(ex.Message);
                ShapeValue.isUnitTestValid = false;
            }
        }

        public void DrawRect(int width, int height)
        {
            try { 
            int xpos = x - (width / 2);
            int ypos = y - (height / 2);
            CurrShape = new Rectangle(xpos, ypos, width, height);
                if (ShapeValue.isFill)
                {
                    this.g.FillRectangle(ShapeValue.FillColor, CurrShape);
                    this.g.DrawRectangle(shapeBorder, CurrShape);
                }
                else
                {
                    this.g.DrawRectangle(ShapeValue.pen, CurrShape);
                }
                ShapeValue.isUnitTestValid = true;
            }
            catch (Exception ex)
            {
                PrintMessage(ex.Message);
                ShapeValue.isUnitTestValid = false;
            }
        }

        public void DrawCircle(int width)
        {
            try { 
            int xpos = x - (width / 2);
            int ypos = y - (width / 2);
            CurrShape = new Rectangle(xpos, ypos, width, width);
                if (ShapeValue.isFill)
                {
                    this.g.FillEllipse(ShapeValue.FillColor, CurrShape);
                    this.g.DrawEllipse(shapeBorder, xpos, ypos, width, width);
                }
                else
                    this.g.DrawEllipse(ShapeValue.pen, xpos, ypos, width, width);
                ShapeValue.isUnitTestValid = true;
            }
            catch (Exception ex)
            {
                PrintMessage(ex.Message);
                ShapeValue.isUnitTestValid = false;
            }
        }

        public void MovePoint(int xpos, int ypos)
        {
            try { 
            Pen pen = new Pen(SystemColors.ActiveBorder, 2);
            this.g.DrawRectangle(pen, CuurPos);
            pen = new Pen(Color.Red, 2);
            CuurPos = GetRectangle(xpos, ypos, 2, 2);
            this.g.DrawRectangle(pen, CuurPos);
            x = ShapeValue.x = xpos;
            y = ShapeValue.y = ypos;
            
                ShapeValue.isUnitTestValid = true;
            }
            catch (Exception ex)
            {
                PrintMessage(ex.Message);
                ShapeValue.isUnitTestValid = false;
            }
        }

        public void DrawTriangle(int xpos, int ypos, int zpos)
        {
            try { 
            int tx, ty, cx, cy;
            cx = Convert.ToInt32(x - (xpos / 3))  ;
            cy = Convert.ToInt32(y - (ypos / 3)) ;
            tx = Convert.ToInt32(cx + xpos);
            ty = Convert.ToInt32(cy + ypos);  

            Point[] points = new Point[3];
            points[0] = new Point(cx, cy);
            points[1] = new Point(tx, cy);
            points[2] = new Point(cx, ty);
            if(ShapeValue.isFill)
            {
                this.g.FillPolygon(ShapeValue.FillColor, points);
                this.g.DrawPolygon(shapeBorder, points);
            }
            else
                this.g.DrawPolygon(ShapeValue.pen, points);
            ShapeValue.isUnitTestValid = true;
            }
            catch (Exception ex)
            {
                PrintMessage(ex.Message);
                ShapeValue.isUnitTestValid = false;
            }
        }

        public void CurrPoint(Boolean flg)
        {
            Pen pen = new Pen(Color.Red, 2);
            if (flg)
            { 
                CuurPos = GetRectangle(ShapeValue.x, ShapeValue.y, 2, 2);
                this.g.DrawRectangle(pen, CuurPos);
            }
            else
            {
                x = y = 0;
                ShapeValue.x = ShapeValue.y = 0;
                CuurPos = GetRectangle(x, y, 2, 2);
                this.g.DrawRectangle(pen, CuurPos);
            }            
        }

        public void PrintMessage(String ErrMsg)
        {
            using (Font myFont = new Font("Arial", 14))
            {
                g.DrawString(ErrMsg, myFont, Brushes.Black, new Point(5, 5));
            }
        }

        private Rectangle GetRectangle(int rx, int ry, int rwidth, int rheight)
        {
            return new Rectangle(rx, ry, rwidth, rheight);
        }
    }
}
