﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Assignment_1
{
    public class DrawCircle : Shapes
    {
        public string drawShape(int x,int y)
        {
            try
            {
                int xpos = ShapeValue.x - (x / 2);
                int ypos = ShapeValue.y - (x / 2);
                ShapeValue.CurrShape = new Rectangle(xpos, ypos, x, x);
                if (ShapeValue.isFill)
                {
                    ShapeValue.g.FillEllipse(ShapeValue.FillColor, ShapeValue.CurrShape);
                    ShapeValue.g.DrawEllipse(ShapeValue.shapeBorder, xpos, ypos, x, x);
                }
                else
                    ShapeValue.g.DrawEllipse(ShapeValue.pen, xpos, ypos, x, x);
                ShapeValue.isUnitTestValid = true;
                return "";
            }
            catch (Exception ex)
            {                
                ShapeValue.isUnitTestValid = false;
                return ex.Message.Trim();
            }
        }
    }
}
