﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_1
{
    public class Drawline : Shapes
    {
        public string drawShape(int x, int y)
        {
            try
            {
                ShapeValue.g.DrawLine(ShapeValue.pen, ShapeValue.x, ShapeValue.y, x, y);
                ShapeValue.isUnitTestValid = true;
                return "";
            }
            catch (Exception ex)
            {                
                ShapeValue.isUnitTestValid = false;
                return ex.Message.Trim();
            }
        }
    }
}
