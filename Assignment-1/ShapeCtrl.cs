﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Assignment_1
{
    public class ShapeCtrl
    {
        //creating variable to use in current class
        string errMsg = string.Empty, cond_key = string.Empty;
        //Array to store wole string of textbox 
        string[] cmdLines;
        //Condithon valuu to check condition statments
        int cond_Value = 0;
        //create variable of shape interface
        Shapes s;
        //create list variable to store single statments
        List<string> statmentList;
        //list to stroe variable statment
        List<KeyValuePair<string, int>> declaration;
        //list to store comands which seperated from multiline statments
        List<KeyValuePair<string, string>> cmdList;
        
        //Function to validate number with two argument
        //1st is string in which number pass and
        //2nd is reference variable in which we set number
        private Boolean checkNumber(string no, ref int val)
        {
            //boolean variable isnumber to validate number 
            Boolean isNumber = false;
            //check number if is true then return true and set number in reference variable val 
            if (int.TryParse(no.Trim(), out val))
                isNumber = true;
            else
            {
                //Checking declaration list if number is in declaration list then return its value other wise return false 
                //which indicates invalid number
                foreach (var sublist in declaration)
                {
                    if (sublist.Key.ToString().Trim().ToLower() == no.ToString().Trim().ToLower())
                    {
                        val = sublist.Value;
                        isNumber = true;
                    }
                }
            }
            return isNumber;
        }

        //function executting multiline command statment
        //1st argument is multiline cpmmand statment
        //2nd is flag to for checking syntex and execution
        public void RunMultiCommands(string str, Boolean runFlg)
        {
            //Checking statment end properly
            Boolean endif, endfor, endwhile;
            //variable to storing single line from multistatment command 
            string singleLine = string.Empty;
            //variable to store single command 
            string[] singlecmd;
            //declaring variable statmentlist, declaration, and cmdlist
            statmentList = new List<string>();
            declaration = new List<KeyValuePair<string, int>>();
            cmdList = new List<KeyValuePair<string, string>>();
            //two integer variable to manage commands 
            int cmdx = 0, k = 0;
            //Regex to checking format of Loop and if statments
            Regex var, assign_var, f_loop, w_loop, if_stat;
            var = new Regex(@"^[A-Za-z]+\s[=]\s\d{0,3}$");
            assign_var = new Regex(@"^[A-Za-z]+\s[=]\s[A-Za-z]+$");
            f_loop = new Regex(@"^(loop)\s(for)\s\d{0,3}$");
            w_loop = new Regex(@"^(while)\s[A-Za-z]+\s(<|>|=|>=|<=)\s\d{0,3}$");
            if_stat = new Regex(@"^(if)\s[A-Za-z]+\s(<|>|=|>=|<=)\s\d{0,3}$");

            //split multicommand statment into smdLine
            cmdLines = str.ToLower().Split(new string[] { "\n" }, StringSplitOptions.None);
            //Loop to execute all command line by line 
            for (int i = 0; i < cmdLines.Count(); i++)
            {
                //check current line is empty or not
                if (cmdLines[i].Trim().ToString() != string.Empty)
                {
                    //replace multiple space and trim command from both side 
                    singleLine = Regex.Replace(cmdLines[i].ToString().ToLower().Trim(), @"\s+", " ");
                    //check is formate like variable
                    if (var.IsMatch(singleLine))
                    {
                        //if its variable then split command using space
                        singlecmd = singleLine.Trim().Split(' ');
                        //check command contain three value
                        if (singlecmd.Count() == 3)
                        {
                            //check for number in command
                            if (checkNumber(singlecmd[2].ToString().Trim(), ref cmdx))
                            {
                                //After checking all posibilities store value in List variable
                                declaration.Add(new KeyValuePair<string, int>(singlecmd[0].ToString(), cmdx));
                                //reset cmdx
                                cmdx = 0;
                            }
                            else
                            {
                                //write error msg if number is invalid
                                errMsg = errMsg + " Invalid number at line " + (i + 1).ToString() + "!\n";
                                runFlg = false;
                            }
                        }
                        else
                        {
                            //writte error msg if command argument not matched
                            errMsg = errMsg + " Invalid command at line " + (i + 1).ToString() + "!\n";
                            runFlg = false;
                        }
                    }
                    //check formate for assign value like 'var = radius'
                    else if (assign_var.IsMatch(singleLine))
                    {
                        //split multicommand statment into smdLine
                        singlecmd = singleLine.Trim().Split(' ');
                        //check command contain three value                        
                        if (singlecmd.Count() == 3)
                        {
                            //Loop to execute for all existing variable
                            foreach (var sublist in declaration)
                            {                                
                                if (sublist.Key.ToString().Trim().ToLower() == singlecmd[2].ToString().Trim().ToLower())
                                {
                                    //check if varibale declare then store its value to new variable
                                    declaration.Add(new KeyValuePair<string, int>(singlecmd[0].ToString(), sublist.Value));
                                }
                                {
                                    //if variable is not in declaration list then throw error
                                    errMsg = errMsg + " Invalid variable at command line " + (i + 1).ToString() + "!\n";
                                    runFlg = false;
                                }
                            }
                        }
                        else
                        {
                            //validating statment argument
                            errMsg = errMsg + " Invalid command at line " + (i + 1).ToString() + "!\n";
                            runFlg = false;
                        }
                    }
                    //Checking for loop
                    else if (f_loop.IsMatch(singleLine))
                    {
                        //split multicommand statment into smdLine
                        singlecmd = singleLine.Trim().Split(' ');
                        //check command contain three value                        
                        if (singlecmd.Count() == 3)
                        {
                            //check number
                            if (checkNumber(singlecmd[2].Trim().ToString(), ref cmdx))
                            {
                                k = i + 1;
                                //if number is validate then run loop as per condition
                                for (int j = 1; j <= cmdx; j++)
                                {
                                    i = k;
                                    //replace multiline space into single space andstoe command into single line
                                    singleLine = Regex.Replace(cmdLines[i].ToString().ToLower().Trim(), @"\s+", " ");
                                    //run code until loop end
                                    while (!Regex.IsMatch(singleLine, @"^(end)\s(for)$"))
                                    {
                                        //execute all code in for loop line by line
                                        RunCommand(singleLine, i, false, ref runFlg);
                                        //check is last line or not if not then move to second line otherwise brek code
                                        if (i < (cmdLines.Count() - 1))
                                            i++;
                                        else
                                            break;
                                        //replace multiline space into single space andstoe command into single line
                                        singleLine = Regex.Replace(cmdLines[i].ToString().ToLower().Trim(), @"\s+", " ");
                                        //check loop end successfully
                                        if (Regex.IsMatch(singleLine, @"^(end)\s(for)$"))
                                        {
                                            endfor = true;
                                        }
                                    }
                                    //break loop if user check only syntex
                                    if (!runFlg)
                                        break;
                                }
                            }
                            else
                            {
                                //throw error if is not number
                                errMsg = errMsg + " Invalid number at line " + (i + 1).ToString() + "!\n";
                                runFlg = false;
                            }
                        }
                        else
                        {
                            //validate command argument 
                            errMsg = errMsg + " Invalid command at line " + (i + 1).ToString() + "!\n";
                            runFlg = false;
                        }
                    }
                    //Checking while loop
                    else if (w_loop.IsMatch(singleLine))
                    {
                        //split single line into by space
                        singlecmd = singleLine.Trim().Split(' ');
                        //checking argument as per command
                        if (singlecmd.Count() == 4)
                        {
                            //check for nuber in command 
                            if (checkNumber(singlecmd[3].Trim().ToString(), ref cmdx))
                            {
                                k = i + 1;
                                //check variable in declaration for loop condition
                                foreach (var sublist in declaration.ToList())
                                {
                                    //declare variable wrong command
                                    Boolean Wrong_cond = true;
                                    //assign condition value and key to execute loop condition
                                    cond_Value = sublist.Value;
                                    cond_key = sublist.Key;
                                    //check variable is declared or not
                                    if (sublist.Key.ToString().Trim().ToLower() == singlecmd[1].ToString().Trim().ToLower())
                                    {
                                        //checking for condition expression
                                        switch (singlecmd[2].ToString().Trim())
                                        {
                                            case "=":
                                                //execute loop code as per condition
                                                while (cond_Value == cmdx)//command line loop condition
                                                {
                                                    i = k;
                                                    //execute loop by calling this fuction
                                                    LoopExecution(singleLine, @"^(endwhile)$", ref i, ref runFlg);
                                                    //set if condition is wrong
                                                    Wrong_cond = false;
                                                    //break if user wnt to check only syntex
                                                    if (!runFlg)
                                                        break;
                                                }
                                                //if wrong condition then check end loop statment 
                                                if (Wrong_cond)
                                                {
                                                    i = k;
                                                    //check syntex of loop if condition is wrong
                                                    LoopWrongCond(singleLine, @"^(endwhile)$", ref i);
                                                }
                                                break;                                            
                                            case "<=":
                                                while (cond_Value <= cmdx)
                                                {
                                                    i = k;
                                                    LoopExecution(singleLine, @"^(endwhile)$", ref i, ref runFlg);
                                                    Wrong_cond = false;
                                                    if (!runFlg)
                                                        break;
                                                }
                                                if (Wrong_cond)
                                                {
                                                    i = k;
                                                    LoopWrongCond(singleLine, @"^(endwhile)$", ref i);
                                                }
                                                break;
                                            case ">=":
                                                while (cond_Value >= cmdx)
                                                {
                                                    i = k;
                                                    LoopExecution(singleLine, @"^(endwhile)$", ref i, ref runFlg);
                                                    Wrong_cond = false;
                                                    if (!runFlg)
                                                        break;
                                                }
                                                if (Wrong_cond)
                                                {
                                                    i = k;
                                                    LoopWrongCond(singleLine, @"^(endwhile)$", ref i);
                                                }
                                                break;
                                            case "<":
                                                while (cond_Value < cmdx)
                                                {
                                                    i = k;
                                                    LoopExecution(singleLine, @"^(endwhile)$", ref i, ref runFlg);
                                                    Wrong_cond = false;
                                                    if (!runFlg)
                                                        break;
                                                }
                                                if (Wrong_cond)
                                                {
                                                    i = k;
                                                    LoopWrongCond(singleLine, @"^(endwhile)$", ref i);
                                                }
                                                break;
                                            case ">":
                                                while (cond_Value > cmdx)
                                                {
                                                    i = k;
                                                    LoopExecution(singleLine, @"^(endwhile)$", ref i, ref runFlg);
                                                    Wrong_cond = false;
                                                    if (!runFlg)
                                                        break;
                                                }
                                                if (Wrong_cond)
                                                {
                                                    i = k;
                                                    LoopWrongCond(singleLine, @"^(endwhile)$", ref i);
                                                }
                                                break;
                                            default:
                                                //throw error of invalidate expression if not matc any expression
                                                errMsg = errMsg + " Invalid expression at line " + (i + 1).ToString() + "!\n";
                                                runFlg = false;
                                                break;
                                        }
                                    }
                                    //reset variable
                                    cond_Value = 0;
                                    cond_key = string.Empty;
                                }
                            }
                            else
                            {
                                //throw error if number is invalide
                                errMsg = errMsg + " Invalid number at line " + (i + 1).ToString() + "!\n";
                                runFlg = false;
                            }
                        }
                        else
                        {
                            //throw error for invalidate command
                            errMsg = errMsg + " Invalid command at line " + (i + 1).ToString() + "!\n";
                            runFlg = false;
                        }
                    }
                    //checking for if statments
                    else if (if_stat.IsMatch(singleLine))
                    {
                        //split single line into by space
                        singlecmd = singleLine.Trim().Split(' ');
                        //checking argument as per command
                        if (singlecmd.Count() == 4)
                        {
                            //validating number
                            if (checkNumber(singlecmd[3].Trim().ToString(), ref cmdx))
                            {
                                k = i + 1;
                                //check for varibale in declaration
                                foreach (var sublist in declaration.ToList())
                                {
                                    //reset value for conditon key and value
                                    cond_key = sublist.Key;
                                    cond_Value = sublist.Value;
                                    //check if variable is available in declaration
                                    if (sublist.Key.ToString().Trim().ToLower() == singlecmd[1].ToString().Trim().ToLower())
                                    {
                                        //check for all possible expression 
                                        switch (singlecmd[2].ToString().Trim())
                                        {
                                            case "=":
                                                //check multiline command condition 
                                                if (cond_Value == cmdx)
                                                {
                                                    i = k;
                                                    //if condition is true then execute if statment code
                                                    LoopExecution(singleLine, @"^(endif)$", ref i, ref runFlg);
                                                }
                                                else
                                                {
                                                    i = k;
                                                    //if condition false then check syntex 
                                                    LoopWrongCond(singleLine, @"^(endif)$", ref i);
                                                }
                                                break;
                                            case "<=":
                                                if (cond_Value <= cmdx)
                                                {
                                                    i = k;
                                                    LoopExecution(singleLine, @"^(endif)$", ref i, ref runFlg);
                                                }
                                                else
                                                {
                                                    i = k;
                                                    LoopWrongCond(singleLine, @"^(endif)$", ref i);
                                                }
                                                break;
                                            case ">=":
                                                if (cond_Value >= cmdx)
                                                {
                                                    i = k;
                                                    LoopExecution(singleLine, @"^(endif)$", ref i, ref runFlg);
                                                }
                                                else
                                                {
                                                    i = k;
                                                    LoopWrongCond(singleLine, @"^(endif)$", ref i);
                                                }
                                                break;
                                            case "<":
                                                if (cond_Value < cmdx)
                                                {
                                                    i = k;
                                                    LoopExecution(singleLine, @"^(endif)$", ref i, ref runFlg);
                                                }
                                                else
                                                {
                                                    i = k;
                                                    LoopWrongCond(singleLine, @"^(endif)$", ref i);
                                                }
                                                break;
                                            case ">":
                                                if (cond_Value > cmdx)
                                                {
                                                    i = k;
                                                    LoopExecution(singleLine, @"^(endif)$", ref i, ref runFlg);
                                                }
                                                else
                                                {
                                                    i = k;
                                                    LoopWrongCond(singleLine, @"^(endif)$", ref i);
                                                }
                                                break;
                                            default:
                                                errMsg = errMsg + " Invalid expression at line " + (i + 1).ToString() + "!\n";
                                                runFlg = false;
                                                break;
                                        }
                                    }
                                    //reset value
                                    cond_Value = 0;
                                    cond_key = string.Empty;
                                }
                            }
                            else
                            {
                                //throw if number is invalidate
                                errMsg = errMsg + " Invalid number at line " + (i + 1).ToString() + "!\n";
                                runFlg = false;
                            }
                        }
                        else
                        {
                            //throw error if no of argument is not matched
                            errMsg = errMsg + " Invalid command at line " + (i + 1).ToString() + "!\n";
                            runFlg = false;
                        }
                    }
                    else
                    {
                        //run if all statment are not matched
                        RunCommand(singleLine, i, false, ref runFlg);
                    }
                }
            }
            //check error message is not empty and runflag is false then prin error messages
            if (errMsg.Trim() != string.Empty && runFlg == false)
            {                
                PrintMessage(errMsg);
            }
        }

        public void RunCommand(string singleLine, int lineno, Boolean print_flg, ref Boolean runFlg)
        {
            //variable for single command
            string[] singlecmd;
            //variable for command status if command is execute or not
            Boolean cmdStatus = false;
            //variable for pointx and pointy
            int cmdx = 0, cmdY = 0;
            //split comand and store in singlecmd array
            singlecmd = singleLine.Trim().Split(' ');
            //check command for move 
            if (singlecmd[0].ToString().Trim().Equals("move"))
            {
                //if its move then check for number of argumne tin command
                if (singlecmd.Count() != 3)
                {
                    //if  argument not matched then throw exeption
                    errMsg = errMsg + "Command line " + lineno.ToString() + " is invalid!\n";
                    //set runflag as false 
                    runFlg = false;
                    //check print flag if is not true then return
                    if (!print_flg)
                        return;
                }
                else
                {
                    //check number if argument is matched
                    if (checkNumber(singlecmd[1].Trim(), ref cmdx))
                    {
                        //check other number 
                        if (checkNumber(singlecmd[2].Trim(), ref cmdY))
                        {
                           //if bothe nuber are valid and runflag is true then declare object of class MoveTO
                            if (runFlg)
                                s = new MoveTo();
                            //set command status as true
                            cmdStatus = true;
                        }
                        else
                        {
                            //throw error if is not number and set runflag = false ang return 
                            errMsg = errMsg + " Invalid number at command line " + (lineno + 1).ToString() + "!\n";
                            runFlg = false;
                            if (!print_flg)
                                return;
                        }
                    }
                    else
                    {
                        //throw error if is not number and set runflag = false ang return 
                        errMsg = errMsg + " Invalid number at command line " + (lineno + 1).ToString() + "!\n";
                        runFlg = false;
                        if (!print_flg)
                            return;
                    }
                }
            }
            //if command for line
            else if (singlecmd[0].ToString().Trim().Equals("line"))
            {
                //check no of argument
                if (singlecmd.Count() != 3)
                {
                    //throw error if argument not matched
                    errMsg = errMsg + "Command line " + lineno.ToString() + " is invalid!\n";
                    runFlg = false;
                    if (!print_flg)
                        return;
                }
                else
                {
                    //check number validation
                    if (checkNumber(singlecmd[1].Trim(), ref cmdx))
                    {
                        //check number validation
                        if (checkNumber(singlecmd[2].Trim(), ref cmdY))
                        {
                            //check runflag if is true then declare Drwaline object
                            if (runFlg)
                                s = new Drawline();
                            //set cmd status as true
                            cmdStatus = true;
                        }
                        else
                        {
                            //throw error if is not number and set runflag = false ang return 
                            errMsg = errMsg + " Invalid number at command line " + (lineno + 1).ToString() + "!\n";
                            runFlg = false;
                            if (!print_flg)
                                return;
                        }
                    }
                    else
                    {
                        //throw error if is not number and set runflag = false ang return 
                        errMsg = errMsg + " Invalid number at command line " + (lineno + 1).ToString() + "!\n";
                        runFlg = false;
                        if (!print_flg)
                            return;
                    }
                }
            }
            else if (singlecmd[0].ToString().Trim().Equals("tri"))
            {
                if (singlecmd.Count() != 3)
                {
                    errMsg = errMsg + "Command line " + lineno.ToString() + " is invalid!\n";
                    runFlg = false;
                    if (!print_flg)
                        return;
                }
                else
                {
                    if (checkNumber(singlecmd[1].Trim(), ref cmdx))
                    {
                        if (checkNumber(singlecmd[2].Trim(), ref cmdY))
                        {
                            if (runFlg)
                                s = new DrawTriangle();

                            cmdStatus = true;
                        }
                        else
                        {
                            //throw error if is not number and set runflag = false ang return 
                            errMsg = errMsg + " Invalid number at command line " + (lineno + 1).ToString() + "!\n";
                            runFlg = false;
                            if (!print_flg)
                                return;
                        }
                    }
                    else
                    {
                        //throw error if is not number and set runflag = false ang return 
                        errMsg = errMsg + " Invalid number at command line " + (lineno + 1).ToString() + "!\n";
                        runFlg = false;
                        if (!print_flg)
                            return;
                    }
                }
            }
            else if (singlecmd[0].ToString().Trim().Equals("rect"))
            {
                if (singlecmd.Count() != 3)
                {
                    errMsg = errMsg + "Command line " + lineno.ToString() + " is invalid!\n";
                    runFlg = false;
                    if (!print_flg)
                        return;
                }
                else
                {
                    if (checkNumber(singlecmd[1].Trim(), ref cmdx))
                    {
                        if (checkNumber(singlecmd[2].Trim(), ref cmdY))
                        {
                            if (runFlg)
                                s = new DrawRectangle();

                            cmdStatus = true;
                        }
                        else
                        {
                            //throw error if is not number and set runflag = false ang return 
                            errMsg = errMsg + " Invalid number at command line " + (lineno + 1).ToString() + "!\n";
                            runFlg = false;
                            if (!print_flg)
                                return;
                        }
                    }
                    else
                    {
                        //throw error if is not number and set runflag = false ang return 
                        errMsg = errMsg + " Invalid number at command line " + (lineno + 1).ToString() + "!\n";
                        runFlg = false;
                        if (!print_flg)
                            return;
                    }
                }
            }
            else if (singlecmd[0].ToString().Trim().Equals("cir"))
            {
                if (singlecmd.Count() != 2)
                {
                    errMsg = errMsg + "Command line " + lineno.ToString() + " is invalid!\n";
                    runFlg = false;
                    if (!print_flg)
                        return;
                }
                else
                {
                    if (checkNumber(singlecmd[1].Trim(), ref cmdx))
                    {
                        if (runFlg)
                            s = new DrawCircle();

                        cmdStatus = true;
                    }
                    else
                    {
                        //throw error if is not number and set runflag = false ang return 
                        errMsg = errMsg + " Invalid number at command line " + (lineno + 1).ToString() + "!\n";
                        runFlg = false;
                        if (!print_flg)
                            return;
                    }
                }
            }
            else if (singlecmd[0].ToString().Trim().Equals("squ"))
            {
                if (singlecmd.Count() != 2)
                {
                    errMsg = errMsg + "Command nine " + lineno.ToString() + " is invalid!\n";
                    runFlg = false;
                    if (!print_flg)
                        return;
                }
                else
                {
                    if (checkNumber(singlecmd[1].Trim(), ref cmdx))
                    {
                        if (runFlg)
                            s = new DrawSquare();

                        cmdStatus = true;
                    }
                    else
                    {
                        //throw error if is not number and set runflag = false ang return 
                        errMsg = errMsg + " Invalid number at command line " + (lineno + 1).ToString() + "!\n";
                        runFlg = false;
                        if (!print_flg)
                            return;
                    }
                }
            }
            else
            {
                bool rnline = false;
                foreach (var sublist in declaration.ToList())
                {
                    if (singlecmd.Count() == 5)
                    {
                        if (sublist.Key.ToString().Trim().ToLower() == singlecmd[2].ToString().Trim().ToLower() &&
                            sublist.Key.ToString().Trim().ToLower() == singlecmd[2].ToString().Trim().ToLower())
                        {
                            switch (singlecmd[3].ToString().Trim().ToLower())
                            {
                                case "+":
                                    if (checkNumber(singlecmd[4].ToString().Trim(), ref cmdx))
                                    {
                                        if (cond_key.ToString() == sublist.Key)
                                        {
                                            cond_Value = cond_Value + cmdx;
                                        }
                                        declaration.Add(new KeyValuePair<string, int>(sublist.Key.ToString(), (sublist.Value + cmdx)));
                                        declaration.Remove(sublist);
                                        rnline = true;
                                        cmdx = 0;
                                    }
                                    else
                                    {
                                        //throw error if is not number and set runflag = false ang return 
                                        errMsg = errMsg + " Invalid number at line " + (lineno + 1).ToString() + "!\n";
                                        runFlg = false;
                                        if (!print_flg)
                                            return;
                                    }
                                    break;
                                case "-":
                                    if (checkNumber(singlecmd[4].ToString().Trim(), ref cmdx))
                                    {
                                        if (cond_key.ToString() == sublist.Key)
                                        {
                                            cond_Value = cond_Value + cmdx;
                                        }
                                        declaration.Add(new KeyValuePair<string, int>(sublist.Key.ToString(), (sublist.Value - cmdx)));
                                        declaration.Remove(sublist);
                                        rnline = true;
                                        cmdx = 0;
                                    }
                                    else
                                    {
                                        //throw error if is not number and set runflag = false ang return 
                                        errMsg = errMsg + " Invalid number at line " + (lineno + 1).ToString() + "!\n";
                                        runFlg = false;
                                        if (!print_flg)
                                            return;
                                    }
                                    break;
                                default:
                                    errMsg = errMsg + " Invalid Expression at line " + (lineno + 1).ToString() + "!\n";
                                    runFlg = false;
                                    if (!print_flg)
                                        return;
                                    break;
                            }
                        }
                    }
                }
                if (!rnline)
                {
                    errMsg = errMsg + " Invalid command at line " + (lineno + 1).ToString() + "!\n";
                    runFlg = false;
                    if (!print_flg)
                        return;
                }
            }            
            if (cmdStatus)
            {
                //execute command id command status is true and store value in msg
                string msg = s.drawShape(cmdx, cmdY);
                //check if command execute successfully by checking msg value as empty 
                //and if is not executed then throe error which generated by exception handling
                if(msg.Trim() != string.Empty)                    
                    errMsg = errMsg + "\n Error at line no - " + lineno.ToString() + " " + msg.Trim() + "!\n";
            }
            if (errMsg.Trim() != string.Empty && runFlg == false && print_flg)
            {
                //print messgae if errmessage is not empty, runflag is false and print flag is true
                PrintMessage(errMsg);
            }
            if (ShapeValue.isFill)
            {
                //retrive current point if shape is fill by any color
                CurrPoint(true);
            }
        }

        private void LoopExecution(string singleLine, string end_statmet, ref int lineno, ref Boolean runflg)
        {
            //replace multispace as single space
            singleLine = Regex.Replace(cmdLines[lineno].ToString().ToLower().Trim(), @"\s+", " ");
            //execute statment until end statment not found
            while (!Regex.IsMatch(singleLine, end_statmet))
            {
                //run command 
                RunCommand(singleLine, lineno, false, ref runflg);
                //check if is not last line then move o next line otherwise break the loop
                if (lineno < (cmdLines.Count() - 1))
                    lineno++;
                else
                    break;
                //replace multispace as single space after moving to next line
                singleLine = Regex.Replace(cmdLines[lineno].ToString().ToLower().Trim(), @"\s+", " ");
            }
        }
        private void LoopWrongCond(string singleLine, string end_statmet, ref int lineno)
        {
            singleLine = Regex.Replace(cmdLines[lineno].ToString().ToLower().Trim(), @"\s+", " ");
            while (!Regex.IsMatch(singleLine, end_statmet))
            {
                if (lineno < (cmdLines.Count() - 1))
                    lineno++;
                else
                    break;
                singleLine = Regex.Replace(cmdLines[lineno].ToString().ToLower().Trim(), @"\s+", " ");
            }
        }

        public void PrintMessage(String ErrMsg)
        {
            //set font for message
            using (Font myFont = new Font("Arial", 14))
            {
                //print messge in picture box
                ShapeValue.g.DrawString(ErrMsg, myFont, Brushes.Black, new Point(5, 5));
            }
        }

        public void CurrPoint(Boolean flg)
        {
            //declare new pen
            Pen pen = new Pen(Color.Red, 2);            
            if (flg)
            {
                //if user press clear button then just clear all control and print new point on same position
                ShapeValue.CuurPos = new Rectangle(ShapeValue.x, ShapeValue.y, 2, 2);
                ShapeValue.g.DrawRectangle(pen, ShapeValue.CuurPos);
            }
            else
            {
                //if user press reset button then clear all control and reset current point as initial point
                ShapeValue.x = ShapeValue.y = 0;
                ShapeValue.CuurPos = new Rectangle(ShapeValue.x, ShapeValue.y, 2, 2);
                ShapeValue.g.DrawRectangle(pen, ShapeValue.CuurPos);
            }
        }

        public void TeestAssgin()
        {
            //function for assigm value for unit testing
            ShapeValue.pen = new Pen(Color.Black, 1);
            ShapeValue.x = ShapeValue.y = 0;

            ShapeValue.g = Graphics.FromImage(ShapeValue.NewPicture);
            ShapeValue.shapeBorder = new Pen(Color.Black, 1);

        }


        //public void runCommands(String strtxt)
        //{
        //    string errMsg = string.Empty;
        //    string strCommand = string.Empty;
        //    Boolean runFlg = true;
        //    int cmdX = 0, cmdY = 0, cmdz = 0;
        //    string[] arrCommand = strtxt.ToLower().Split(new string[] { ";" }, StringSplitOptions.None);
        //    string[] oneCommand;

        //    for (int i = 0; i < arrCommand.Count(); i++)
        //    {
        //        if (arrCommand[i].Trim().ToString() != string.Empty)
        //        {
        //            oneCommand = arrCommand[i].Trim().Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
        //            for (int j = 0; j < oneCommand.Count(); j++)
        //            {
        //                if (oneCommand[j].ToString().Trim().Equals("move"))
        //                {
        //                    if (oneCommand.Count() != 3)
        //                    {
        //                        errMsg = errMsg + "Command line " + (i + 1).ToString() + " is invalid!\n";
        //                        runFlg = false;
        //                        break;
        //                    }
        //                    else
        //                    {
        //                        if (checkNumber(oneCommand[j + 1].Trim(), ref cmdX))
        //                        {
        //                            if (checkNumber(oneCommand[j + 2].Trim(), ref cmdY))
        //                            {
        //                                if (runFlg)
        //                                {
        //                                    MovePoint(cmdX, cmdY);
        //                                }
        //                            }
        //                            else
        //                            {
        //                                errMsg = errMsg + " Invalid number at command line " + (i + 1).ToString() + "!\n";
        //                                runFlg = false;
        //                            }
        //                        }
        //                        else
        //                        {
        //                            errMsg = errMsg + " Invalid number at command line " + (i + 1).ToString() + "!\n";
        //                            runFlg = false;
        //                        }
        //                        j = j + 2;
        //                    }
        //                }
        //                else if (oneCommand[j].ToString().Trim().Equals("line"))
        //                {
        //                    if (oneCommand.Count() != 3)
        //                    {
        //                        errMsg = errMsg + "Command line " + i.ToString() + " is invalid!\n";
        //                        runFlg = false;
        //                        break;
        //                    }
        //                    else
        //                    {
        //                        if (checkNumber(oneCommand[j + 1].Trim(), ref cmdX))
        //                        {
        //                            if (checkNumber(oneCommand[j + 2].Trim(), ref cmdY))
        //                            {
        //                                if (runFlg)
        //                                {
        //                                    DrawLine(cmdX, cmdY);
        //                                }
        //                            }
        //                            else
        //                            {
        //                                errMsg = errMsg + " Invalid number at command line " + i.ToString() + "!\n";
        //                                runFlg = false;
        //                            }
        //                        }
        //                        else
        //                        {
        //                            errMsg = errMsg + " Invalid number at command line " + i.ToString() + "!\n";
        //                            runFlg = false;
        //                        }
        //                        j = j + 2;
        //                    }
        //                }
        //                else if (oneCommand[j].ToString().Trim().Equals("cir"))
        //                {
        //                    if (oneCommand.Count() != 2)
        //                    {
        //                        errMsg = errMsg + "Command line " + (i + 1).ToString() + " is invalid!\n";
        //                        runFlg = false;
        //                        break;
        //                    }
        //                    else
        //                    {
        //                        if (checkNumber(oneCommand[j + 1].Trim(), ref cmdX))
        //                        {
        //                            if (runFlg)
        //                            {
        //                                DrawCircle(cmdX);
        //                            }
        //                        }
        //                        else
        //                        {
        //                            errMsg = errMsg + " Invalid number at command line " + (i + 1).ToString() + "!\n";
        //                            runFlg = false;
        //                        }
        //                        j = j + 1;
        //                    }
        //                }
        //                else if (oneCommand[j].ToString().Trim().Equals("squ"))
        //                {
        //                    if (oneCommand.Count() != 2)
        //                    {
        //                        errMsg = errMsg + "Command line " + (i + 1).ToString() + " is invalid!\n";
        //                        runFlg = false;
        //                        break;
        //                    }
        //                    else
        //                    {
        //                        if (checkNumber(oneCommand[j + 1].Trim(), ref cmdX))
        //                        {
        //                            if (runFlg)
        //                            {
        //                                DrawSquare(cmdX);
        //                            }
        //                        }
        //                        else
        //                        {
        //                            errMsg = errMsg + " Invalid number at command line " + (i + 1).ToString() + "!\n";
        //                            runFlg = false;
        //                        }
        //                        j = j + 1;
        //                    }
        //                }
        //                else if (oneCommand[j].ToString().Trim().Equals("rect"))
        //                {
        //                    if (oneCommand.Count() != 3)
        //                    {
        //                        errMsg = errMsg + "Invalid no of argument at command " + (i + 1).ToString() + "!\n";
        //                        runFlg = false;
        //                        break;
        //                    }
        //                    else
        //                    {
        //                        if (checkNumber(oneCommand[j + 1].Trim(), ref cmdX))
        //                        {
        //                            if (checkNumber(oneCommand[j + 2].Trim(), ref cmdY))
        //                            {
        //                                if (runFlg)
        //                                {
        //                                    DrawRect(cmdX, cmdY);
        //                                }
        //                            }
        //                            else
        //                            {
        //                                errMsg = errMsg + " Invalid number at command line " + (i + 1).ToString() + "!\n";
        //                                runFlg = false;
        //                            }
        //                        }
        //                        else
        //                        {
        //                            errMsg = errMsg + " Invalid number at command line " + (i + 1).ToString() + "!\n";
        //                            runFlg = false;
        //                        }
        //                        j = j + 2;
        //                    }
        //                }
        //                else if (oneCommand[j].ToString().Trim().Equals("tri"))
        //                {
        //                    if (oneCommand.Count() != 4)
        //                    {
        //                        errMsg = errMsg + "Command line " + (i + 1).ToString() + " is invalid!\n";
        //                        runFlg = false;
        //                        break;
        //                    }
        //                    else
        //                    {
        //                        if (checkNumber(oneCommand[j + 1].Trim(), ref cmdX))
        //                        {
        //                            if (checkNumber(oneCommand[j + 2].Trim(), ref cmdY))
        //                            {
        //                                if (checkNumber(oneCommand[j + 3].Trim(), ref cmdz))
        //                                {
        //                                    if (runFlg)
        //                                    {
        //                                        DrawTriangle(cmdX, cmdY, cmdz);
        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    errMsg = errMsg + " Invalid number at command line " + (i + 1).ToString() + "!\n";
        //                                    runFlg = false;
        //                                }
        //                            }
        //                            else
        //                            {
        //                                errMsg = errMsg + " Invalid number at command line " + (i + 1).ToString() + "!\n";
        //                                runFlg = false;
        //                            }
        //                        }
        //                        else
        //                        {
        //                            errMsg = errMsg + " Invalid number at command line " + (i + 1).ToString() + "!\n";
        //                            runFlg = false;
        //                        }
        //                        j = j + 3;
        //                    }
        //                }
        //                else
        //                {
        //                    errMsg = errMsg + "Command line " + (i + 1).ToString() + " is invalid!\n";
        //                    runFlg = false;
        //                    break;
        //                }

        //            }
        //        }
        //    }
        //    if (errMsg.Trim() != string.Empty)
        //    {
        //        PrintMessage(errMsg);
        //    }
        //    if (ShapeValue.isFill)
        //    {
        //        CurrPoint(true);
        //    }
        //}

        //Function to 
    }
}
